/**
 * Bài tập 1
 * Input: nhập điểm chuẩn, điểm từng môn và điểm ưu tiên
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến diemChuan, diemKhuVuc, diemDoiTuong, diemMon1, diemMon2, diemMon3 theo giá trị của HTML element
 *    + Khai báo và gán biến diemTong = 0
 *    + Gán biến diemTong = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong
 *    + Dùng cấu trúc if-else:
 *        Nếu có ít nhất điểm một môn nhỏ hơn 0 thì in dòng chữ "Bạn đã rớt do có điểm thành phần nhỏ hơn hoặc bằng 0"
 *        Nếu không thì:
 *           Nếu diemTong lớn hơn hoặc bằng diemChuan thì in dòng chữ "Bạn đã đậu"  và tổng điểm
 *           Nếu không thì in dòng chữ "Bạn đã rớt"  và tổng điểm
 *
 * Output: kết quả đậu hay rớt và tổng điểm
 */
 
function baiTap1() {
  var hoTen = document.getElementById("ho-ten").value;
  var tongThuNhapNam = document.getElementById("tong-thu-nhap").value * 1;
  var soNguoiPhuThuoc = document.getElementById("so-nguoi-phu-thuoc").value * 1;
  var thuNhapChiuThue, tienThue;
  
  thuNhapChiuThue = tongThuNhapNam - 4e+6 -soNguoiPhuThuoc * 1.6e+6;

  if (thuNhapChiuThue <= 60) {
    tienThue = thuNhapChiuThue * 0.05;
  }
  else if (thuNhapChiuThue <= 120) {
    tienThue = 60 * 0.05 + (thuNhapChiuThue - 60) * 0.1;
  }
  else if (thuNhapChiuThue <= 210) {
    tienThue = 60 * 0.05 + (120 - 60) * 0.1 + (thuNhapChiuThue - 120) * 0.15;
  }
  else if (thuNhapChiuThue <= 384) {
    tienThue = 60 * 0.05 + (120 - 60) * 0.1 + (210 -120) * 0.15 + (thuNhapChiuThue - 210) * 0.2;
  }
  else if (thuNhapChiuThue <= 624) {
    tienThue = 60 * 0.05 + (120 - 60) * 0.1 + (210 -120) * 0.15 + (384 - 210) * 0.2 + (thuNhapChiuThue - 384) * 0.25;
  }
  else if (thuNhapChiuThue <= 960) {
    tienThue = 60 * 0.05 + (120 - 60) * 0.1 + (210 -120) * 0.15 + (384 - 210) * 0.2 + (624 - 384) * 0.25 + (thuNhapChiuThue - 624) * 0.3;
  }
  else {
    tienThue = 60 * 0.05 + (120 - 60) * 0.1 + (210 -120) * 0.15 + (384 - 210) * 0.2 + (624 - 384) * 0.25 + (960 - 624) * 0.3 + (thuNhapChiuThue - 960) * 0.35;
  }
  var tienThueVND = Intl.NumberFormat().format(tienThue);
  document.getElementById("result-b1").innerHTML = `Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${tienThueVND}VND`;

}

/**
 * Bài tập 2
 * Input: chọn loại khách hàng (KH) và nhập mã KH, số kênh cao cấp, số kênh kết nối (nếu có)
 *
 * Các bước thực hiện:
 *    + Tạo hàm hiển thị ô nhập số kết nối khi loại KH là doanh nghiệp
 *    + Check xem ô loại KH đã được chọn chưa, nếu chưa thì in dòng chữ "Vui lòng chọn loại khách hàng"
 *    + Khai báo và gán biến maKH, soKenhCaoCap, soKetNoi theo giá trị của HTML element
 *    + Khai báo và gán biến user bằng giá trị của ô chọn loại KH
 *    + Khai báo biến phiHoaDon, phiDichVu, phiKenhCaoCap, tongTien
 *    + Khai báo và gán biến phiKetNoi = 5
 *    + Dùng cấu trúc switch-case cho biến user:
 *       -user = "khachNhaDan":
 *           Gán biến phiHoaDon = 4.5 
 *           Gán biến phiDichVu = 20.5
 *           Gán biến phiKenhCaoCap = 7.5
 *           Gán biến tongTien = phiHoaDon + phiDichVu + phiKenhCaoCap * soKenhCaoCap
 *           In kết quả ra giao diện web    
 *       -user = "khachDoanhNghiep":
 *           Gán biến phiHoaDon = 15
 *           Gán biến phiKenhCaoCap = 50
 *           Dùng cấu trúc if-else:
 *              Nếu soKetNoi <= 10 thì phiDichVu = 75
 *              TH còn lại: phiDichVu = 75 + phiKetNoi * (soKetNoi - 10)
 *           Gán biến tongTien = phiHoaDon + phiDichVu + phiKenhCaoCap * soKenhCaoCap
 *           In kết quả ra giao diện web
 *
 * Output: tổng tiền cáp theo loại KH đã chọn
 */
function khachNhaDan() {
  document.getElementById("ket-noi").style.display = "none";
}

function khachDoanhNghiep() {
  document.getElementById("ket-noi").style.display = "block";
}

function baiTap2() {
  var userOptionEl = document.querySelector('input[name="selector"]:checked');
  if (userOptionEl == null) {
    document.getElementById("result-b2").innerText = `Vui lòng chọn loại khách hàng`;
    return;
  }
  var maKH = document.getElementById("ma-khach-hang").value;
  var soKenhCaoCap = document.getElementById("kenh-cao-cap").value * 1;
  var soKetNoi = document.getElementById("ket-noi").value * 1;
  var user = userOptionEl.value;
  var phiHoaDon, phiDichVu, phiKenhCaoCap, tongTien; 
  var phiKetNoi = 5;
  switch (user) {
    case "khachNhaDan": {
      phiHoaDon = 4.5;
      phiDichVu = 20.5;
      phiKenhCaoCap = 7.5;
      tongTien = phiHoaDon + phiDichVu + phiKenhCaoCap * soKenhCaoCap;
      document.getElementById("result-b2").innerHTML = `Mã khách hàng: ${maKH}; Tổng tiền cáp: $${tongTien}`;
      break
    }
    case "khachDoanhNghiep": {
      phiHoaDon = 15;
      phiKenhCaoCap = 50;
      if (soKetNoi <= 10) {
        phiDichVu = 75;
      }
      else {
        phiDichVu = 75 + phiKetNoi * (soKetNoi - 10);
      }
      tongTien = phiHoaDon + phiDichVu + phiKenhCaoCap * soKenhCaoCap;
      document.getElementById("result-b2").innerHTML = `Mã khách hàng: ${maKH}; Tổng tiền cáp: $${tongTien}`;
      break
    }
  }
}


